![Maven Central](https://img.shields.io/maven-central/v/${project.groupId}/${project.artifactId}.svg)
[![Build Status](https://img.shields.io/jenkins/s/http/services.tidalwave.it/ci/job/${tidalwave.ci.view}_Build_from_Scratch.svg)](${project.ciManagement.url})
[![Test Status](https://img.shields.io/jenkins/t/http/services.tidalwave.it/ci/job/${tidalwave.ci.view}.svg)](${project.ciManagement.url})
[![Coverage](https://img.shields.io/jenkins/c/http/services.tidalwave.it/ci/job/${tidalwave.ci.view}.svg)](${project.ciManagement.url})

${project.name}
================================

${project.description}

${project.name} requires and is tested with JDKs in the range ${tft.maven.requiredJdk}, with target ${tft.javac.release}.

It is released under the [Apache Licence v2](https://www.apache.org/licenses/LICENSE-2.0.txt).

Please have a look at the [project website](${project.url}) for a quick introduction with samples, tutorials, JavaDocs and build reports.


Bootstrapping
-------------

In order to build the project, run from the command line:

```shell
mkdir ${project.artifactId}
cd ${project.artifactId}
git clone ${scm.repo.readonly.url} .
mvn -DskipTests
```

The project can be opened with a recent version of the [IntelliJ IDEA](https://www.jetbrains.com/idea/), 
[Apache NetBeans](https://netbeans.apache.org/) or [Eclipse](https://www.eclipse.org/ide/) IDEs.


Contributing
------------

Pull requests are accepted via [Bitbucket](${scm.repo.readonly.url}) or [GitHub](${tidalwave.github.readonly.url}). There are some guidelines which will make 
applying pull requests easier:

* No tabs: please use spaces for indentation.
* Respect the code style.
* Create minimal diffs — disable 'on save' actions like 'reformat source code' or 'organize imports' (unless you use the IDEA specific configuration for 
  this project).
* Provide [TestNG](https://testng.org/doc/) tests for your changes and make sure your changes don't break any existing tests by running
```mvn clean test```. You can check whether there are currently broken tests at the [Continuous Integration](${project.ciManagement.url}) page.

If you plan to contribute on a regular basis, please consider filing a contributor license agreement. Contact us for
 more information.


Additional Resources
--------------------

* [Issue tracking](${project.issueManagement.url})
* [Continuous Integration](${project.ciManagement.url})
* [Tidalwave Homepage](${project.organization.url})
